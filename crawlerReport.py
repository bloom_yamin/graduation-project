# This module is the first step to download company's reports from the given webpage, 
#I got company's security codes and other basic info which are presented as Excel from CSMAR database, 
#then I used crawler technology to download the relative annual reports and save them locally 
#according to Excel documents.

import os      
import pandas as pd 
import requests
import re
from lxml import etree #resolve web
from urllib.request import urlretrieve #download internet documents to local
import time 

#set the local address to save reports
os.chdir(r"./crawler reoprts") 
os.getcwd()


#This is the process of crawlering the reports for 2015, every year has a special excel document 
#named as "ANN_SecurityXXXX" format, so I can replace the different year's files to get annual reports.
data = pd.read_excel("ANN_Security2015.xlsx")[2:] 

#"http://static.cninfo.com.cn/finalpage/" is the public web to download China public firm's announcements
data["pdf_url"] = "http://static.cninfo.com.cn/finalpage/" + data["DeclareDate"] + "/" + data["AnnouncementID"] + ".PDF" # get report pdf link for cninfo web
for index, row in data.iterrows():  
    name = row["Symbol"] + "_" + row["DeclareDate"] + ".pdf" #document name
    print(name)
    url = row["pdf_url"]       #pdf address
    times = 1                  #if download fails, it will try again
    while times <= 5:          #if it fails more than 5 times, it will end the loop
        try:
            urlretrieve(url, filename =  r"./PDF2015/" + name)
            print(f"Downloaded {name} successfully！")
            break
        except:
            times += 1 
            print(f"sleep 5 seconds！Then try {times} time!")
            time.sleep(5)
print("Have downloaded all pdfs！") 